package main

import (
	"fmt"
	"time"

	"gitlab.com/Elevarup/ele_shorts/observer/internal/observer"
)

func main() {
	s := observer.NewSubject()

	list := []observer.Observer{
		observer.NewCustomer("aa", "aa@a"),
		observer.NewCustomer("bb", "bb@b"),
		observer.NewCustomer("cc", "cc@c"),
		observer.NewCustomer("dd", "dd@d"),
	}

	for _, o := range list {
		s.Subscribe(o)
	}

	now := time.Now()
	s.Notify()

	fmt.Println("main, time since: ", time.Since(now).Seconds())
}
