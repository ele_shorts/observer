package observer

import "sync"

type (
	Observer interface {
		Send()
		GetID() string
	}
	Subject interface {
		Subscribe(o Observer)
		UnSubscribe(o Observer)
		Notify()
	}
	Item struct {
		list map[string]Observer
	}
)

func NewSubject() *Item {
	return &Item{list: make(map[string]Observer)}
}

func (i *Item) Subscribe(o Observer) {
	i.list[o.GetID()] = o
}
func (i *Item) UnSubcribe(o Observer) {
	delete(i.list, o.GetID())
}

func (i *Item) Notify() {
	var wg sync.WaitGroup

	for _, o := range i.list {
		wg.Add(1)

		go func(w *sync.WaitGroup, o Observer) {
			defer w.Done()

			o.Send()
		}(&wg, o)
	}
	wg.Wait()
}
