package observer

import (
	"log"
	"math/rand"
	"time"
)

type (
	Customer struct{ id, email string }
)

func NewCustomer(id, email string) *Customer {
	return &Customer{id, email}
}

func (c *Customer) GetID() string {
	return c.id
}

func (c *Customer) Send() {
	now := time.Now()
	chaos := rand.Intn(5)

	time.Sleep(time.Duration(chaos) * time.Second)

	log.Println("id: ", c.id, " --- send, time since (seconds): ", time.Since(now).Seconds())
}
